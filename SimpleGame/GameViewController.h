//
//  GameViewController.h
//  SimpleGame
//

//  Copyright (c) 2014 IdeaStudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import "GameScene.h"
#import <GameKit/GameKit.h>

@interface GameViewController : UIViewController <GKGameCenterControllerDelegate>
{
    GameScene *scene;
    UIViewController *authenticationController;
}
- (void)reasonableToShowAuthenticationDialog;
- (void) authenticateLocalPlayer;
- (void)showLeaderboard;
- (void) reportScore: (int64_t) score forLeaderboardID: (NSString*) category;
@property BOOL forceAutenticate;
@end
