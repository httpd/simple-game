//
//  GameViewController.m
//  SimpleGame
//
//  Created by Piotrek on 27.10.2014.
//  Copyright (c) 2014 IdeaStudio. All rights reserved.
//

#import "GameViewController.h"
#define MINE
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

@end

@implementation GameViewController
- (void)viewWillLayoutSubviews
{
    self.forceAutenticate = false;
    [self authenticateLocalPlayer];
    
    // Configure the view.
    SKView * skView = (SKView *)self.view;
    
    if (!skView.scene) {
//        skView.showsFPS = YES;
//        skView.showsNodeCount = YES;
//        skView.showsQuadCount = YES;
//        skView.showsDrawCount = YES;

//        skView.frameInterval = 8;
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView.ignoresSiblingOrder = YES;
        skView.multipleTouchEnabled = NO;
        skView.exclusiveTouch = YES;
        if ([skView respondsToSelector:@selector(shouldCullNonVisibleNodes)]) {
            //TODO: DEBUG OPTIONS
            skView.shouldCullNonVisibleNodes = NO; //set to yes if there are no shadows in final version

        }
        // Create and configure the scene.
        CGSize size = skView.bounds.size;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            size.width*=0.6;
            size.height*=0.6;

        }else if(IS_IPHONE)
        {
            CGFloat ratio = size.height/size.width;
            CGFloat standardWidth = 320;
            size.width= standardWidth;
            size.height=ratio*standardWidth;
        }
//        size.width*=5;
//        size.height*=5;
        scene = [GameScene sceneWithSize:size];
        scene.backgroundColor = [UIColor blackColor];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        scene.viewController = self;
        // Present the scene.
        [skView presentScene:scene];

    }
    [self reasonableToShowAuthenticationDialog];
    

}
- (void) authenticateLocalPlayer
{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        
        authenticationController = viewController;

        if (viewController != nil)
        {
            NSLog(@"Show autentiaction dialog");
            if (scene.points==0 || self.forceAutenticate) {
                [self reasonableToShowAuthenticationDialog];
            }
            //showAuthenticationDialogWhenReasonable: is an example method name. Create your own method that displays an authentication view when appropriate for your app.
            
        }
        else if ([GKLocalPlayer localPlayer].isAuthenticated)
        {
            NSLog(@"Local player autenticated");

            //authenticatedPlayer: is an example method name. Create your own method that is called after the loacal player is authenticated.
//            [self authenticatedPlayer: localPlayer];
        }
        else
        {
            NSLog(@"Disable game center");

//            [self disableGameCenter];
        }
    };
}
- (void)reasonableToShowAuthenticationDialog
{
    if (authenticationController!=nil) {
        [self presentViewController:authenticationController animated:YES completion:nil];
    }

}
- (void)showLeaderboard
{
    GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController alloc] init];
    if (gameCenterController != nil)
    {
        gameCenterController.gameCenterDelegate = self;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            gameCenterController.leaderboardIdentifier = @"toppointspad";
        } else {
            gameCenterController.leaderboardIdentifier = @"points";
        }

        gameCenterController.leaderboardIdentifier = @"points";
        gameCenterController.viewState = GKGameCenterViewControllerStateLeaderboards;
//        gameCenterController.leaderboardTimeScope = GKLeaderboardTimeScopeToday;
//        gameCenterController.leaderboardCategory = @"points";
        [self presentViewController: gameCenterController animated: YES completion:nil];
    }

}
- (void) reportScore: (int64_t) score forLeaderboardID: (NSString*) category
{
    NSLog(@"ScoreReporting %lld %@", score, category);
    GKScore *scoreReporter = [[GKScore alloc] initWithLeaderboardIdentifier:category player:[GKLocalPlayer localPlayer]];
    scoreReporter.value = score;
    scoreReporter.context = 0;
    [GKScore reportScores:@[scoreReporter] withCompletionHandler:^(NSError *error){
        NSLog(@"Error reporting scores %@", error);
    }];
    
    
    
}
- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}
- (BOOL)shouldAutorotate
{
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
