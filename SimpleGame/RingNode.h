//
//  RingNode.h
//  CatchTheSun
//
//  Created by Zbyszek on 15.08.2015.
//  Copyright © 2015 IdeaStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

@interface RingNode : SKSpriteNode
@property (nonatomic) CGFloat progress;
@property SKShapeNode *progressBar;
- (void)test;
@end
