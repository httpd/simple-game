//
//  PowerupNode.m
//  SimpleGame
//
//  Created by Piotrek on 27.03.2015.
//  Copyright (c) 2015 IdeaStudio. All rights reserved.
//

#import "PowerupNode.h"
#import "UIColor+Hex.h"
@interface PowerupNode()
@property SKShapeNode *progressBar;

@end
@implementation PowerupNode

- (id)init{
    self = [super init];
    self.size = CGSizeMake(30, 30);
    
    self.progress = 0.f;
    
    CGMutablePathRef circlePath1 = CGPathCreateMutable();
    
    CGPathAddArc(circlePath1, NULL, 0, 0, 13, 0, M_PI*2, NO);

    
    SKShapeNode *shape = [SKShapeNode node];
    shape.path = circlePath1;
    shape.fillColor = [UIColor colorWithCode:0xf2f2f2];
    shape.lineWidth = 4;
    shape.strokeColor = [UIColor colorWithCode:0x2d2d2d];
    [self addChild:shape];
    
    
    
    [self drawProgress];
    return self;
}
- (id)initWithType: (enum powerup)p
{
    self = self.init;
    _type = p;
    
    SKTexture *tex;
    if (p==shield) {
        tex =[SKTexture textureWithImageNamed:@"shield.png"];
    }else if(p==slowmo)
    {
        tex =[SKTexture textureWithImageNamed:@"slowmo.png"];
    }else if (p==bonus) {
        
        tex =[SKTexture textureWithImageNamed:@"points.png"];

    }else if (p==bomb) {
        tex = [SKTexture textureWithImageNamed:@"bomb.png"];
    }

    
    
    SKSpriteNode *icon = [SKSpriteNode node];
    icon.texture = tex;
    icon.size = tex.size;
    [self addChild:icon];

    
    return self;
}
- (void)test
{
    self.duration = 2;
    SKAction *test = [SKAction customActionWithDuration:self.duration actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        PowerupNode *pow = (PowerupNode *)node;
        pow.progress  = elapsedTime/self.duration;
        [pow drawProgress];
    }];
    
    [self runAction:test];
}
- (void)drawProgress
{
    CGMutablePathRef circlePath2 = CGPathCreateMutable();
    
    CGPathAddArc(circlePath2, NULL, 0, 0, 11, M_PI/2, M_PI*(0.5 + 2*self.progress), NO);
    if (!self.progressBar) {
        self.progressBar = [SKShapeNode node];
        [self addChild:self.progressBar];
    }
    self.progressBar.path = circlePath2;
    self.progressBar.lineWidth = 4;
    self.progressBar.strokeColor = [UIColor colorWithCode:0x2d2d2d];

}
@end
