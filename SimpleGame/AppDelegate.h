//
//  AppDelegate.h
//  SimpleGame
//
//  Created by Piotrek on 27.10.2014.
//  Copyright (c) 2014 IdeaStudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

