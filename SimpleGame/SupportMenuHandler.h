//
//  SupportMenuHandler.h
//  CatchTheSun
//
//  Created by Piotrek on 14.08.2015.
//  Copyright (c) 2015 IdeaStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
@interface SupportMenuHandler : NSObject <UIActionSheetDelegate, SKProductsRequestDelegate,
    SKPaymentTransactionObserver>
{
    SKProductsRequest *pRequest;
    NSString *productIdentifier;
    SKProduct *product;
    BOOL productRequest;
    void (^RequestBlock)();
}
@end
