//
//  UIColor+Hex.m
//  Save the ship!
//
//  Created by Piotrek on 01.05.2013.
//
//

#import "UIColor+Hex.h"

@implementation UIColor (Hex)

- (NSUInteger)colorCode
{
    CGFloat red, green, blue;

    if ([self getRed:&red green:&green blue:&blue alpha:NULL])
    {
        NSUInteger redInt = (NSUInteger)(red * 255 + 0.5);
        NSUInteger greenInt = (NSUInteger)(green * 255 + 0.5);
        NSUInteger blueInt = (NSUInteger)(blue * 255 + 0.5);
        
        return (redInt << 16) | (greenInt << 8) | blueInt;
    }
    
    return 0;
}
+ (UIColor *)colorWithCode:(NSUInteger)code
{
    float red, green, blue;
    
    NSInteger blueInt = code & ((1<<8)-1);
    NSInteger greenInt = code >> 8 & ((1<<8)-1);
    NSInteger redInt = code >> 16 & ((1<<8)-1);
    
    red = (redInt-0.5) / 255;
    green = (greenInt-0.5) / 255;
    blue = (blueInt-0.5) / 255;

    return [UIColor colorWithRed:red green:green blue:blue alpha:1];
    
}
+ (UIColor *)lighterColorForColor:(UIColor *)c
{
    CGFloat r, g, b, a;
    if ([c getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:MIN(r + 0.1, 1.0)
                               green:MIN(g + 0.1, 1.0)
                                blue:MIN(b + 0.1, 1.0)
                               alpha:a];
    return nil;
}

+ (UIColor *)darkerColorForColor:(UIColor *)c
{
    CGFloat r, g, b, a;
    if ([c getRed:&r green:&g blue:&b alpha:&a])
        return [UIColor colorWithRed:MAX(r - 0.2, 0.0)
                               green:MAX(g - 0.2, 0.0)
                                blue:MAX(b - 0.2, 0.0)
                               alpha:a];
    return nil;
}
+ (UIColor *)colorBeetweenColor: (UIColor *)color1 andColor:(UIColor *)color2 factor: (CGFloat)factor{
    CGFloat hue1, hue2, sat1, sat2, brig1, brig2;
    [color1 getHue:&hue1 saturation:&sat1 brightness:&brig1 alpha:nil];
    
    [color2 getHue:&hue2 saturation:&sat2 brightness:&brig2 alpha:nil];
    
    CGFloat ehue, esat, ebrig;
    
    ehue = hue1 + (hue2-hue1)*factor;
    esat = sat1 + (sat2-sat1)*factor;
    ebrig = brig1 + (brig2-brig1)*factor;
    
    UIColor *inter = [UIColor colorWithHue:ehue saturation:esat brightness:ebrig alpha:1];
    
    return inter;
}
+ (UIColor *)beterColorBeetweenColor: (UIColor *)color1 andColor:(UIColor *)color2 factor: (CGFloat)p{
    CGFloat r1, g1, b1, r2, b2, g2;
    [color1 getRed:&r1 green:&g1 blue:&b1 alpha:nil ];
    
    [color2 getRed:&r2 green:&g2 blue:&b2 alpha:nil];
    
    CGFloat er, eg, eb;
    
    er = sqrt((1-p)*r1*r1 + p * r2*r2);
    eg = sqrt((1-p)*g1*g1 + p * g2*g2);
    eb = sqrt((1-p)*b1*b1 + p * b2*b2);
    
    UIColor *inter = [UIColor colorWithRed:er green:eg blue:eb alpha:1];
    
    return inter;
}

@end