//
//  SupportMenuHandler.m
//  CatchTheSun
//
//  Created by Piotrek on 14.08.2015.
//  Copyright (c) 2015 IdeaStudio. All rights reserved.
//

#import "SupportMenuHandler.h"
#import "Appirater.h"
@implementation SupportMenuHandler
- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    if (product==nil) {
        pRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:@"thanks_one"]];
        pRequest.delegate = self;
        [pRequest start];

    }
    
    NSLog(@"will present action sheet");
}

- (void)actionSheet:(UIActionSheet *)actionSheet
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        [Appirater rateApp];

    }
    if (buttonIndex==1) {
        __unsafe_unretained typeof(self) weakSelf = self;
        RequestBlock = ^{
            SKPayment * payment = [SKPayment paymentWithProduct:weakSelf->product];
            [[SKPaymentQueue defaultQueue] addPayment:payment];
            [[SKPaymentQueue defaultQueue] addTransactionObserver:weakSelf];
            productRequest = false;

        };
        productRequest = true;
        if (product!=nil) {
            RequestBlock();
        }
        
    }
}
- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response
{

    NSLog(@"Loaded list of products...");
    NSArray * skProducts = response.products;
    for (SKProduct * skProduct in skProducts) {
        NSLog(@"Found product: %@ %@ %0.2f",
              skProduct.productIdentifier,
              skProduct.localizedTitle,
              skProduct.price.floatValue);
    }
    product = skProducts.firstObject;
    if (productRequest) {
        RequestBlock();
    }
}
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"request error %@", error.description);
}
- (void)requestDidFinish:(SKRequest *)request
{
    NSLog(@"Request fisnished");
    
    
}
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self completeTransaction:transaction];
            default:
                break;
        }
    };
}
- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"completeTransaction...");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thank you" message:@"Thank you for supporting development of Catch the Sun." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
    [alert show];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"failedTransaction...");
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Transaction error" message:transaction.error.localizedDescription delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
        [alert show];

    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}
@end
