//
//  PowerupNode.h
//  SimpleGame
//
//  Created by Piotrek on 27.03.2015.
//  Copyright (c) 2015 IdeaStudio. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
enum powerup
{
    slowmo = 1<<0,
    shield = 1<<1,
    bonus = 1<<2,
    bomb = 1<<3,

};

@interface PowerupNode : SKSpriteNode
@property (nonatomic) CGFloat progress;
@property (nonatomic, readonly) enum powerup type;
@property NSTimeInterval duration;
- (void)test;
- (id)initWithType: (enum powerup)p;
@end
