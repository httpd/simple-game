//
//  RingNode.m
//  CatchTheSun
//
//  Created by Zbyszek on 15.08.2015.
//  Copyright © 2015 IdeaStudio. All rights reserved.
//

#import "RingNode.h"
#import "UIColor+Hex.h"

@implementation RingNode
- (void)test
{
    double duration = 0.5;
    SKAction *test = [SKAction customActionWithDuration:duration actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        RingNode *pow = (RingNode *)node;
        pow.progress  = elapsedTime/duration;
        [pow drawProgress];
    }];
    
    [self runAction:test completion:^(void){
        [self removeFromParent];
    }];
}
- (void)drawProgress
{
    CGMutablePathRef circlePath2 = CGPathCreateMutable();
    
    CGPathAddArc(circlePath2, NULL, 0, 0, 11+100*self.progress, 0 , M_PI*2, YES);
    if (!self.progressBar) {
        self.progressBar = [SKShapeNode node];
        [self addChild:self.progressBar];
    }
    self.progressBar.path = circlePath2;
    self.progressBar.lineWidth = 3;
    self.progressBar.strokeColor = [UIColor colorWithCode:0xfdb917];
    self.alpha = 1-self.progress;
}

@end
