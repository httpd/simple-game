//
//  UIColor+Hex.h
//  Save the ship!
//
//  Created by Piotrek on 01.05.2013.
//
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)
- (NSUInteger)colorCode;
+ (UIColor *)colorWithCode: (NSUInteger)code;
+ (UIColor *)lighterColorForColor:(UIColor *)c;
+ (UIColor *)darkerColorForColor:(UIColor *)c;
+ (UIColor *)colorBeetweenColor: (UIColor *)color1 andColor:(UIColor *)color2 factor: (CGFloat)factor;
+ (UIColor *)beterColorBeetweenColor: (UIColor *)color1 andColor:(UIColor *)color2 factor: (CGFloat)p;
@end
