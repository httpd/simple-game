//
//  GameScene.m
//  SimpleGame
//
//  Created by Piotrek on 27.10.2014.
//  Copyright (c) 2014 IdeaStudio. All rights reserved.
//

#import "GameScene.h"
#import "CGVectorAdditions.h"
#import "UIColor+Hex.h"
#import <CoreImage/CoreImage.h>
#import "GameViewController.h"
#import "Flurry.h"
#import "RingNode.h"
#define MINE
//#define LIGHTSON

#define BUG_LIMIT 100
@interface ColorScheme : SKNode
@property () UIColor *backgroundColor;
@property () UIColor *enemyColor;
@property () UIColor *squareColor;
@end
#define bg(c) _backgroundColor = [UIColor colorWithCode:c]
#define sq(c) _squareColor = [UIColor colorWithCode:c]

@implementation ColorScheme

- (instancetype)initWithSchem: (int)schemeNum
{
    self = [super init];
    if (self) {
        //default color
        _backgroundColor = [UIColor colorWithCode:0x2F507F]; //0x2C3E50
        _enemyColor = [UIColor redColor];
        _squareColor = [UIColor colorWithCode:0xECF0F1]; //0xECF0F1
        if (schemeNum==-1) {
            //insane mode
            bg(0xdb4105);
            sq(0xECF0F1);
        }else if (schemeNum==0) {
            bg(0x2F507F); //0x2C3E50
            sq(0xECF0F1); //0xECF0F1
        }else if (schemeNum==1) {
            bg(0x779324); //ok
            sq(0x1d4b6d);
        }else if (schemeNum==2) {
            bg(0x8c517b); //ok
            sq(0x3a3a3a);
            
        }else if(schemeNum == 3)
        {
            bg(0x0071bc);
            sq(0xff7f66);
            
        }else if(schemeNum ==4 )
        {
            bg(0x3da4be);
            sq(0xb6d83a);

        }else if(schemeNum == 5)
        {
            bg(0x9dc02e);
            sq(0x516019);

        }else if(schemeNum == 6)
        {
            bg(0xd8d3c0);
            sq(0x646c6d);


        }else if(schemeNum == 7)
        {
            
            bg(0x7f3300);
            sq(0x56264b);

        }else
        {
            bg(0x182240);
        }
        
        
    }
    return self;
}
@end
@implementation GameScene
@synthesize points, topScore, combo;
- (void)setPoints:(int)pts
{
    points = pts;
    if (points>self.topScore) {
        self.topScore = points;
        newTopScore = true;
    }
    if (pointsLabel!=nil) {
        pointsLabel.text = [NSString stringWithFormat:@"%d", pts];
    }
    
}
- (void)setTopScore:(int)ts
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:ts] forKey:@"highScore"];
    [defaults synchronize];
    topScore = ts;
    
    if (topScoreLabel!=nil) {
        topScoreLabel.text = [NSString stringWithFormat:@"★ %d", ts];
    }
}
- (int)topScore
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *pts = [defaults objectForKey:@"highScore"];
    if (pts==nil) {
        topScore = 0;
    }else
        topScore = pts.intValue;
    
    return topScore;
}
- (void)setCombo:(int)cm
{
    if (cm==2) {
        [self addComboLabel];
    }
    if (comboLabel) {
        if (cm==1) {
            [self removeComboLabel];
        }else
            comboLabel.text = [NSString stringWithFormat: @"| speed bonus %d", cm-1];

    }
    combo = cm;
}
-(void)didMoveToView:(SKView *)view {
    
    ivar = self.size;
    //#ifdef MINE
    //    self.scene.backgroundColor = [UIColor colorWithCode:0x2C3E50];
    //#else
    //    self.scene.backgroundColor = [UIColor colorWithCode:0x042A30];
    //
    //#endif
    //    NSLog(@"%@", view.backgroundColor);
    touchdown = NO; //we dont reset touchdown every time
    
    /*
     NSArray *fontFamilies = [UIFont familyNames];
     for (int i = 0; i < [fontFamilies count]; i++)
     {
     NSString *fontFamily = [fontFamilies objectAtIndex:i];
     NSArray *fontNames = [UIFont fontNamesForFamilyName:[fontFamilies objectAtIndex:i]];
     NSLog (@"%@: %@", fontFamily, fontNames);
     }
     */
//    [self setTopScore:0]; //TODO: DEBUG OPTION
    pointsLabel = [SKLabelNode node];
    pointsLabel.text = @"0";
    pointsLabel.position = CGPointMake(30, self.frame.size.height-40);
    pointsLabel.fontName = @"Thonburi";
    pointsLabel.fontSize = 20;
    pointsLabel.fontColor = [UIColor colorWithCode:0xF2F2F2];
    
    topScoreLabel = [SKLabelNode node];
    topScoreLabel.fontName = @"Thonburi";
    topScoreLabel.text = [NSString stringWithFormat:@"★ %d", self.topScore];
    topScoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeRight;
    topScoreLabel.position = CGPointMake(self.frame.size.width-30, self.frame.size.height-40);
    topScoreLabel.fontSize = 20;
    topScoreLabel.fontColor = [UIColor colorWithCode:0xFFE600];
    
    

    
    [self addChild:pointsLabel];
    [self addChild:topScoreLabel];

    
    
    SKLabelNode *score = [SKLabelNode node];
    score.text = @"Best:";
    score.fontName = @"Thonburi";
    score.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeRight;
    score.position = CGPointMake(self.frame.size.width-85, self.frame.size.height-30);
    score.fontSize = 15;
    
    //    [self addChild:score];
    
    
    
    self.container = [[SKSpriteNode alloc] initWithColor:[UIColor clearColor] size:self.frame.size];
    self.container.anchorPoint = CGPointMake(0, 0);
    //    self.container.color = [UIColor blueColor];


    
    
//    UIImage *file = [UIImage imageNamed:@"kol.png"];
//    CIImage *textureImage = [[CIImage alloc] initWithImage:file];
//    
//    
//    CIFilter *filter = [CIFilter filterWithName:@"CIHueAdjust" keysAndValues:kCIInputImageKey, textureImage, nil];
//    [filter setDefaults];
//    [filter setValue:[NSNumber numberWithFloat:357] forKey:@"inputAngle"];
//    
    bugTexture = [SKTexture textureWithImageNamed:@"kol.png"];
    bugTexture_insane = [SKTexture textureWithImage:[UIImage imageNamed:@"kol_insane.png"]];

    
    [self addChild:self.container];
    
    //init sound effects
    soundEffects = [NSMutableDictionary dictionary];
    
    for (int i=0; i<7; i++) {
        SKAction *soundAction =  [SKAction playSoundFileNamed:[NSString stringWithFormat:@"%d.wav", 1+i] waitForCompletion:NO];
        [soundEffects setObject:soundAction forKey:[NSString stringWithFormat:@"%d.wav", 1+i]];
    }
    
    
    
    [self startGame];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    if (![defaults boolForKey:@"tutorial1"]) { //tutorial
        instr = [SKLabelNode node];
        instr.text = @"Drag anywhere on screen";
        instr.fontName = @"Thonburi";
        instr.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
        instr.fontSize = 20;
        instr.color = [UIColor colorWithCode:0xF2F2F2];
        [self addChild:instr];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:true forKey:@"tutorial1"];
        [defaults synchronize];
    }
    
    
}


- (SKSpriteNode *)lineBetweenPoint: (CGPoint)p1 andPoint: (CGPoint)p2 withWidth: (CGFloat)width andColor: (UIColor *)color
{
    SKSpriteNode *node = [SKSpriteNode spriteNodeWithColor:color size:CGSizeMake(width, CGPointDistance(p1, p2))];
    node.anchorPoint = CGPointMake(0.5, 0);
    node.position = p1;
    node.zRotation = CGVectorAngle(CGVectorBetweenPoints(p1, p2))-M_PI/2.f;
    
    
    return node;
    
}
- (void)startGame
{
    
    mode = standard;
    [self.container removeAllChildren];
    self.theGameIsAfoot = true;
    newTopScore = false;
    lastColectionTime = nil;
    flagCount = 0;
    //generate color scheme
    if (mode == insane) {
        scheme = [[ColorScheme alloc] initWithSchem:-1];
    }else
        scheme = [[ColorScheme alloc] initWithSchem:0];
    
    
    
    self.scene.backgroundColor = scheme.backgroundColor;
    
    
    CGPoint lastPos = CGPointMake(100, 100);
    if (self.player) {
        lastPos = self.player.position;
    }
    
    
    self.player = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithRed:0 green:0 blue:0.5 alpha:1 ] size:CGSizeMake(20, 20)];
#ifdef MINE
    self.player = [SKSpriteNode spriteNodeWithColor:scheme.squareColor size:CGSizeMake(20, 20)];
#else
    self.player = [SKSpriteNode spriteNodeWithImageNamed:@"mojludek.png"];
    
#endif
    self.player.position = lastPos;
    if (mode == insane) {
        self.player.position = CGPointMake(100, 100);
    }
    
    self.player.zPosition = 1;
    if([self.player respondsToSelector:@selector(shadowCastBitMask)])
        self.player.shadowCastBitMask = 1;
    
    
    trail = [NSMutableArray array];
    
    velocity = CGVectorMake(0, 0);
    //    if (self.container) {
    //        [self.container removeAllChildren];
    //        self.container = nil;
    //    }
    
    
    self.points = 0;
    
    
    
    [self.container addChild:self.player];
    
    
    //    light = [[SKLightNode alloc] init];
    //    [light setName:@"light"];
    //    [light setPosition:CGPointMake(0, 0)];
    //    [light setCategoryBitMask:1];
    //
    //    [light setFalloff:1.5];
    //    [light setZPosition:0];
    //    [light setAmbientColor:[UIColor greenColor]];
    //    [light setLightColor:[[UIColor alloc] initWithRed:1.0 green:0.0 blue:0.0 alpha:.5]];
    //    [light setShadowColor:[[UIColor alloc] initWithRed:0 green:0 blue:0 alpha:.05]];
    //    [self addChild:light];
#ifdef LIGHTSON
    
    if ([SKLightNode class]) {
        light = [[SKLightNode alloc] init];
        [light setName:@"light"];
        [light setPosition:CGPointMake(0, 0)];
        [light setCategoryBitMask:1];
        light.position = self.flag.position;
        [light setFalloff:0.5];
        [light setZPosition:0];
        [light setAmbientColor:[UIColor whiteColor]];
        [light setLightColor:[[UIColor alloc] initWithRed:1 green:1 blue:1 alpha:.5]];
        [light setShadowColor:[[UIColor alloc] initWithRed:0 green:0 blue:0 alpha:.05]];
        [self.container addChild:light];
    }
    
#endif
    
    
    [self addRandomFlag];
    //    [self.container addChild:self.flag];
    
    bugNum = 0;
    pow = 0;
    self.bugs = [NSArray array];
    self.powerups = [NSMutableArray array];
    //    self.player.size = CGSizeMake(20, 20);
    
    //kolce
    if (mode == insane) {
        int margin = 10;
        SKSpriteNode *kolce = [SKSpriteNode spriteNodeWithImageNamed:@"border.png"];
        for (double i=margin; i<self.size.height-margin*2; i+=kolce.size.height) {
            SKSpriteNode* kolce1 = kolce.copy;
            kolce1.position = CGPointMake(0, i);
            kolce1.anchorPoint = CGPointMake(0, 0);
            
            [self.container addChild:kolce1];
            
            SKSpriteNode *kolce2 = kolce.copy;
            kolce2.position = CGPointMake(self.size.width-kolce.size.width, i);
            kolce2.anchorPoint = CGPointMake(1, 0);
            kolce2.xScale = -1;
            [self.container addChild:kolce2];
            
        }
        kolce = [SKSpriteNode spriteNodeWithImageNamed:@"border_h.png"];
        for (double i=margin; i<self.size.width-2*margin; i+=kolce.size.width) {
            SKSpriteNode* kolce1 = kolce.copy;
            kolce1.position = CGPointMake(i, 0);
            kolce1.anchorPoint = CGPointMake(0, 0);
            
            [self.container addChild:kolce1];
            
            SKSpriteNode *kolce2 = kolce.copy;
            kolce2.position = CGPointMake(i, self.size.height);
            kolce2.anchorPoint = CGPointMake(0, 0);
            kolce2.yScale = -1;
            [self.container addChild:kolce2];
            
        }

    }
    for (int i=0; i<BUG_LIMIT; i++) {
        SKSpriteNode *invisibleBugNode;
        if (mode == insane) {
            invisibleBugNode = [SKSpriteNode spriteNodeWithTexture:bugTexture_insane];
            
        }else
            invisibleBugNode = [SKSpriteNode spriteNodeWithTexture:bugTexture];

        
        if([invisibleBugNode respondsToSelector:@selector(shadowCastBitMask)])
        {
            invisibleBugNode.shadowCastBitMask  = 1;
            invisibleBugNode.lightingBitMask = 1;
            
        }
        invisibleBugNode.position = CGPointMake(-100, -100);
        active[i] = false;
        [[self container] addChild:invisibleBugNode];
        self.bugs = [self.bugs arrayByAddingObject:invisibleBugNode];

    }

    if (mode == insane) {
        addingTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(runAddingTimer:) userInfo:nil repeats:YES];
        
        [addingTimer fire];
    }
    //flurry raport
    [Flurry endTimedEvent:@"game_played" withParameters:nil];
    [self.viewController reasonableToShowAuthenticationDialog];
    
}
- (void)runAddingTimer: (id)userInfo
{
//    NSLog(@"run");
    [self addRandomBug];
}
- (void)addRandomFlag
{
    
    if (self.flag!=nil) {
        [self.flag removeFromParent];
    }
    
#ifdef MINE
    self.flag = [SKSpriteNode spriteNodeWithImageNamed:@"token.png"];
#else
    self.flag = [SKSpriteNode spriteNodeWithImageNamed:@"korona.png"];
    
#endif
    //        self.flag = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:1] size:CGSizeMake(20, 20)];
    
    //    }
    
    
    
    [self.container addChild:self.flag];
    self.flag.zPosition = 0;
    //    NSLog(@"%f %f", self.frame.size.width,self.frame.size.height);
    //    int wx =arc4random_uniform(self.frame.size.width);
    //    int wy =arc4random_uniform(self.frame.size.height);
    
    CGPoint randPoint;
    CGPoint loc = self.player.position;
    
    int dist = 100;
    do {
        randPoint = CGPointMake(arc4random_uniform(self.frame.size.width) , arc4random_uniform(self.frame.size.height));
    } while ((randPoint.x-loc.x)*(randPoint.y-loc.y) < dist*dist );
    
    
    //    [self.flag setPosition:randPoint];
    [self randomlySetPositionSprite:self.flag avoidObject:[@[self.player] arrayByAddingObjectsFromArray:self.powerups]];
    light.position = self.flag.position;
    
    
    //    [self addChild:self.flag];
}
- (void)addRandomBug
{
#ifdef MINE
    SKSpriteNode *bug;

    
    
#else
    
    //    SKSpriteNode *bug = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(20, 20)];
//    SKSpriteNode *bug = [SKSpriteNode spriteNodeWithImageNamed:@"ludek.png"];
    
#endif
    bug = [self.bugs objectAtIndex:bugNum%BUG_LIMIT];
    active[bugNum%BUG_LIMIT] = true;
    if (arc4random_uniform(2)==0) {
        //horizontal bug
        bX[bugNum%BUG_LIMIT] = 10;
        bY[bugNum%BUG_LIMIT] = 0;
    }else
    {
        //vertical bug
        bX[bugNum%BUG_LIMIT] = 0;
        bY[bugNum%BUG_LIMIT] = 10;
        
    }
//    if([bug respondsToSelector:@selector(shadowCastBitMask)])
//    {
//        bug.shadowCastBitMask  = 1;
//        bug.lightingBitMask = 1;
//        
//    }
    CGPoint randPoint;
    CGPoint loc = self.player.position;
    
    int dist = 100;
    do {
        randPoint = CGPointMake(arc4random_uniform(self.frame.size.width) , arc4random_uniform(self.frame.size.height));
    } while ((randPoint.x-loc.x)*(randPoint.y-loc.y) < dist*dist );
    
    //    bug.position = randPoint;
    [self randomlySetPositionSprite:bug avoidObject:@[self.player]];
    
//    [self.container addChild:bug];
    bug.zPosition = 2;
//    self.bugs = [self.bugs arrayByAddingObject:bug];
    
    
    bugNum++;
}

- (void)fixLocation: (SKNode *)node inSize: (CGSize)oldsize constraint: (enum horizontalConstraints)hc :(enum verticalConstraint)vc
{
    CGPoint position = node.position;
    if (hc == hright) {
        CGFloat delta = position.x - oldsize.width;
        position.x = self.size.width + delta;
    }else if (hc == hcenter)
    {
        
    }
    
    if (vc == vtop) {
        CGFloat delta = position.y - oldsize.height;
        position.y = self.size.height + delta;
    }
    node.position = position;
}
//
CGPoint CGPointSwap(CGPoint p, CGSize size)
{
    CGFloat x = p.x;
    p.x = p.y;
    p.y = x;
    
    //    p.x = fabsf(size.width - p.x);
    //    p.y = fabsf(size.height - p.y);
    
    return p;
}
//- (void)didChangeSize:(CGSize)oldSize
//{
//    [super didChangeSize:oldSize];
//
//    NSLog(@"test %f %f", ivar.height, oldSize.height);
//
//    [self fixLocation:pointsLabel inSize:ivar constraint:hleft :vtop];
//    [self fixLocation:topScoreLabel inSize:ivar constraint:hright :vtop];
//
//
//
//    self.container.anchorPoint = CGPointMake(0, 0);
//
//    if (ivar.height != self.size.height &&
//        ivar.width != self.size.width) {
//        self.player.position = CGPointSwap(self.player.position, self.size);
//        self.flag.position = CGPointSwap(self.flag.position, self.size);
//        for (SKNode *node in self.bugs) {
//            node.position = CGPointSwap(node.position,self.size);
//        }
//        for (SKNode *node in self.powerups) {
//            node.position = CGPointSwap(node.position, self.size);
//        }
//        for (__strong NSValue *obj in trail) {
//            CGPoint point = [obj CGPointValue];
//            point = CGPointSwap(point, self.size);
//            obj = [NSValue valueWithCGPoint:point];
//        }
//    }
//
//    ivar = self.size;
//}
- (void)addPowerup
{
    PowerupNode *power = [[PowerupNode alloc]initWithType:slowmo];
    CGPoint randPoint;
    CGPoint loc = self.player.position;
    
    int dist = 100;
    do {
        randPoint = CGPointMake(arc4random_uniform(self.frame.size.width) , arc4random_uniform(self.frame.size.height));
    } while ((randPoint.x-loc.x)*(randPoint.y-loc.y) < dist*dist );
    
    //    power.position = randPoint;
    [self randomlySetPositionSprite:power avoidObject:@[self.player, self.flag]];
    
    SKAction *removeAction = [SKAction runBlock:^(void){
        [power removeFromParent];
        [self.powerups removeObject:power];
    }];
    
    [self.container addChild:power];
    [self.powerups addObject:power];
    [power test];
    [power runAction:[SKAction sequence:@[[SKAction waitForDuration:power.duration], removeAction]]];

}
- (void)addBomb
{
    PowerupNode *power = [[PowerupNode alloc]initWithType:bomb];
    CGPoint randPoint;
    CGPoint loc = self.player.position;
    
    int dist = 100;
    do {
        randPoint = CGPointMake(arc4random_uniform(self.frame.size.width) , arc4random_uniform(self.frame.size.height));
    } while ((randPoint.x-loc.x)*(randPoint.y-loc.y) < dist*dist );
    
    //    power.position = randPoint;
    [self randomlySetPositionSprite:power avoidObject:@[self.player, self.flag]];
    
    SKAction *removeAction = [SKAction runBlock:^(void){
        [power removeFromParent];
        [self.powerups removeObject:power];
    }];
    
    [self.container addChild:power];
    [self.powerups addObject:power];
    [power test];
    [power runAction:[SKAction sequence:@[[SKAction waitForDuration:power.duration], removeAction]]];

}
- (void)powerupColected: (PowerupNode *)powerup
{
    
    
    if (powerup.type == slowmo) {
        PowerupNode *tempToken = [powerup copy];
        [self.container addChild:tempToken];
        

        
        SKAction *dis = [SKAction scaleTo:0 duration:0.5];
        dis.timingMode = SKActionTimingEaseIn;
        SKAction *fade = [SKAction fadeAlphaTo:0 duration:0.3];
        [tempToken runAction:[SKAction group:@[dis, fade]] completion:^(void){ [tempToken removeFromParent]; }];

        
        
        pow |= slowmo;
        [self runAction:[SKAction waitForDuration:4] completion:^(){
            pow &= ~slowmo;
        }];

        SKLabelNode *pointsLabelTemp = [SKLabelNode node];
        pointsLabelTemp.text = @"Slowdown";
        pointsLabelTemp.position = CGPointMake(self.size.width/2, self.size.height/2);
        pointsLabelTemp.alpha = 0;
        pointsLabelTemp.zPosition = 10;
        pointsLabelTemp.fontSize = 40;
        pointsLabelTemp.fontName = @"HelveticaNeue-Bold";
        pointsLabelTemp.fontColor = [UIColor colorWithCode:0xFEE600];
//        [self.container addChild:pointsLabelTemp];
        
        
        SKAction *appear = [SKAction fadeAlphaTo:0.95 duration:0.2];
        SKAction *dissapear = [SKAction fadeAlphaTo:0 duration:0.5];
        SKAction *chain = [SKAction sequence:@[appear, dissapear]];
        SKAction *scale = [SKAction scaleBy:1.3 duration:0.7];
        scale.timingMode = SKActionTimingEaseOut;
        SKAction *array = [SKAction group:@[chain, scale]];
        [pointsLabelTemp runAction:array];

    }else if(powerup.type == bomb){
        
        for (int i=0; i<MIN(bugNum, BUG_LIMIT); i++) {
            SKSpriteNode *bug = [self.bugs objectAtIndex:i];
            if (CGPointDistance(bug.position, powerup.position)<100 && active[i])
            {
                NSString *emiterPath = [[NSBundle mainBundle] pathForResource:@"RedExp" ofType:@"sks"];

                
                SKEmitterNode *emitter = [NSKeyedUnarchiver unarchiveObjectWithFile:emiterPath];
                emitter.position = bug.position;
                //        emitter.numParticlesToEmit = 100;
                //        emitter.particleBirthRate = 5000;
                emitter.zPosition = -1;
                [self.container addChild:emitter];
                [emitter resetSimulation];

                active[i] = false;
                bug.position = CGPointMake(-100, -100);
                
            }
        }
        NSString *emiterPath = [[NSBundle mainBundle] pathForResource:@"BombParticle" ofType:@"sks"];

        SKEmitterNode *emitter = [NSKeyedUnarchiver unarchiveObjectWithFile:emiterPath];
        emitter.position = powerup.position;
//        emitter.numParticlesToEmit = 100;
//        emitter.particleBirthRate = 5000;
        emitter.zPosition = -1;
        [self.container addChild:emitter];
        [emitter resetSimulation];
        
        
    }
    
    [powerup removeFromParent];

}
- (void)showScoreScreen
{
    endPanel = [SKSpriteNode node];
    int margin = 30;
    endPanel.size = CGSizeMake(296, 152);

    endPanel.position = CGPointMake(self.size.width/2, endPanel.size.height/2+self.size.height+margin);
    endPanel.zPosition = 25;

    SKSpriteNode *fill = [SKSpriteNode node];
    fill.texture = [SKTexture textureWithImageNamed:@"panel1@2x.png"];
    fill.size = CGSizeMake(55, 55);

    fill.centerRect = CGRectMake(50.f/110.f, 50.f/100.f, 0.1f, 0.1f);
    fill.xScale = 296/55.f;
    fill.yScale = 152/55.f;
    [endPanel addChild:fill];
    
    
    
    int r = 60;
    SKLabelNode *pkt = [SKLabelNode node];
    pkt.text = [NSString stringWithFormat:@"%d", self.points];
    pkt.fontColor = [UIColor blackColor];
    pkt.fontName = @"HelveticaNeue-Thin";
    pkt.fontSize  = 60;
    pkt.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    pkt.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    pkt.position = CGPointMake(-r, -20);
    pkt.zPosition = 26;
    
    [endPanel addChild:pkt];
    
    SKLabelNode *pkt_label = [SKLabelNode node];
    pkt_label.text =@"score";
    pkt_label.fontColor = [UIColor blackColor];
    pkt_label.fontName = @"HelveticaNeue-Thin";
    pkt_label.fontSize  = 15;
    pkt_label.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    pkt_label.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    pkt_label.position = CGPointMake(-r, 15);
    pkt_label.zPosition = 26;
    
    [endPanel addChild:pkt_label];

    
    SKLabelNode *pkt2 = [SKLabelNode node];
    pkt2.text = [NSString stringWithFormat:@"%d", self.topScore];
    pkt2.fontColor = [UIColor blackColor];
    pkt2.fontName = @"HelveticaNeue-Thin";
    pkt2.fontSize  = 60;
    pkt2.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    pkt2.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    
    pkt2.position = CGPointMake(r, -20);
    pkt2.zPosition = 26;
    [endPanel addChild:pkt2];
    
    SKLabelNode *pkt2_label = [SKLabelNode node];
    pkt2_label.text =@"top score";
    pkt2_label.fontColor = [UIColor blackColor];
    pkt2_label.fontName = @"HelveticaNeue-Thin";
    pkt2_label.fontSize  = 15;
    pkt2_label.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    pkt2_label.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    pkt2_label.position = CGPointMake(r, 15);
    pkt2_label.zPosition = 26;
    
    [endPanel addChild:pkt2_label];
    if (newTopScore) {
        pkt2_label.position = CGPointMake(r+20, 15);

        
        SKLabelNode *pkt2_label_new = [SKLabelNode node];
        pkt2_label_new.text = @"NEW";
        pkt2_label_new.fontName = @"HelveticaNeue-Bold";
        pkt2_label_new.fontSize  = 15;
        pkt2_label_new.position = CGPointMake(r-30.f, 15);
        pkt2_label_new.zPosition = 26;
        pkt2_label_new.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        pkt2_label_new.fontColor = [UIColor colorWithCode:0x779324];
        [endPanel addChild:pkt2_label_new];

    }


    SKLabelNode *play_label = [SKLabelNode node];
    play_label.text =@"Tap anywhere to play again";
    
    play_label.fontColor = [UIColor blackColor];
    play_label.fontName = @"HelveticaNeue-Thin";
    play_label.fontSize  = 8;
    play_label.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    play_label.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    play_label.position = CGPointMake(0, -endPanel.size.height/2+10);
    play_label.zPosition = 26;
    
    [endPanel addChild:play_label];

    SKLabelNode *ego_label = [SKLabelNode node];
    ego_label.text =@"You are better than 150 000 players";
    
    ego_label.fontColor = [UIColor blackColor];
    ego_label.fontName = @"HelveticaNeue-Thin";
    ego_label.fontSize  = 15;
    ego_label.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    ego_label.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    ego_label.position = CGPointMake(0, -endPanel.size.height/2+15);
    ego_label.zPosition = 26;
    
//    [endPanel addChild:ego_label];

    
    SKLabelNode *pkt3 = [SKLabelNode node];
    if (newTopScore) {
        pkt3.text = [NSString stringWithFormat:@"You are the best!"];
    }else
        pkt3.text = [NSString stringWithFormat:@"Try again?"];

    pkt3.fontColor = [UIColor blackColor];
    pkt3.fontName = @"HelveticaNeue-Thin";
    pkt3.fontSize  = 25;
    pkt3.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    pkt3.verticalAlignmentMode = SKLabelVerticalAlignmentModeTop;
    
    pkt3.position = CGPointMake(0, 60);
    pkt3.zPosition = 26;
    [endPanel addChild:pkt3];
    
    
    int er = 135;
    SKLabelNode *leaderboards = [SKLabelNode node];
    leaderboards.text = [NSString stringWithFormat:@"leaderboards"];
    leaderboards.fontColor = [UIColor whiteColor];
    leaderboards.fontName = @"HelveticaNeue-Bold";
    leaderboards.fontSize  = 12;
    leaderboards.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    leaderboards.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    leaderboards.position = CGPointMake(-er, -95);
    leaderboards.zPosition = 26;
    leaderboards.name = @"leaderboards_button";
    
    [endPanel addChild:leaderboards];

    SKLabelNode *support = [SKLabelNode node];
    support.text = [NSString stringWithFormat:@"support the developer"];
    support.fontColor = [UIColor whiteColor];
    support.fontName = @"HelveticaNeue-Bold";
    support.fontSize  = 12;
    support.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeRight;
    support.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    support.position = CGPointMake(er, -95);
    support.zPosition = 26;
    support.name = @"support_button";
    
    [endPanel addChild:support];

    
    [self.container addChild:endPanel];
    SKAction *wait = [SKAction waitForDuration:0.8];
    
    SKAction *present = [SKAction moveToY:self.size.height/2+endPanel.size.height/2 duration:.25];
    present.timingMode = SKActionTimingEaseOut;
    
    SKAction *gCAuth = [SKAction runBlock:^(void){
        [self.viewController reasonableToShowAuthenticationDialog];
    }];
    [endPanel runAction:[SKAction sequence:@[wait, present, gCAuth]] withKey:@"timing"];
    
    
    
    if (newTopScore) {
        NSString *emiterPath = [[NSBundle mainBundle] pathForResource:@"Fireworks" ofType:@"sks"];
        
        for (int i=0; i<5; i++) {
            int pos = arc4random_uniform(300);
//            NSLog(@"pos: %d", pos);
            SKEmitterNode *emitter = [NSKeyedUnarchiver unarchiveObjectWithFile:emiterPath];
            emitter.position = CGPointMake(-150+pos, 100+arc4random()%50);
            emitter.numParticlesToEmit = 100;
            emitter.particleBirthRate = 5000;
            //        emitter.particleScale = 0.2;
            //    emitter.zPosition = 5;
            //    emitter.particleColorSequence = nil;
            //    emitter.particleColorBlendFactor = 1.0;
            //    emitter.particleColor = [scheme squareColor];
            emitter.zPosition = -1;
            if (i==0) {
                [endPanel runAction: [SKAction sequence:@[[SKAction waitForDuration:0.8], [SKAction runBlock:^(void){
                    if (emitter.parent) {
                        [emitter resetSimulation];
                    }else
                        [endPanel addChild:emitter];
                    
                }]]]];
                
            }else
            {
                [endPanel runAction: [SKAction repeatActionForever:[SKAction sequence:@[[SKAction waitForDuration:0.8 +     ((arc4random()%10)/10.f)], [SKAction runBlock:^(void){
                    if (emitter.parent) {
                        [emitter resetSimulation];
                    }else
                        [endPanel addChild:emitter];
                    
                }]]]]];
            }
            //        [endPanel runAction: [SKAction waitForDuration:1.5 + i*0.2] completion:^(void){
            //            [endPanel addChild:emitter];
            //        }];
        }
    }
    
    
}
int signum(int i)
{
    if (i>0) {
        return 1;
    }
    if (i==0) {
        return 0;
    }return -1;
}
- (void)gameOver
{
    //raport flurry
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"points",[NSString stringWithFormat:@"%d", points],
                                   @"high_score", [NSString stringWithFormat:@"%d", self.topScore]
                                   ,nil];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.viewController reportScore:self.topScore forLeaderboardID:@"toppointspad" ];
    } else {
        [self.viewController reportScore:self.topScore forLeaderboardID:@"poins" ];
    }
    [Flurry logEvent:@"game_played" withParameters:articleParams];
    
    //stop insane timer
    if (addingTimer) {
        [addingTimer invalidate];
    }
    //add combo points
    [self comboTimerFire:nil];

    
    //remove all actions on game objects
    [self.player removeActionForKey:@"colorChange"];
    [self removeActionForKey:@"colorChange"];
    
    for (SKNode *node in self.container.children) {
        [node removeActionForKey:@"colorChange"];
    }
    for(SKNode *node in self.powerups)
    {
        [node removeAllActions];
    }
    
    self.theGameIsAfoot = NO;
    
    NSString *emiterPath = [[NSBundle mainBundle] pathForResource:@"MyParticle" ofType:@"sks"];
    SKEmitterNode *emitter = [NSKeyedUnarchiver unarchiveObjectWithFile:emiterPath];
    emitter.position = self.player.position;
    emitter.numParticlesToEmit = 100;
    emitter.particleBirthRate = 3000;
    emitter.particleScale = 0.2;
    emitter.zPosition = 5;
    emitter.particleColorSequence = nil;
    emitter.particleColorBlendFactor = 1.0;
    emitter.particleColor = [scheme squareColor];
    
    [self.container addChild:emitter];
    
    //    [self.player removeFromParent];
//    SKAction *soundAction =  [SKAction playSoundFileNamed:@"test.wav" waitForCompletion:NO];

    SKAction *scale = [SKAction scaleTo:4 duration:0.3];
    SKAction *fade = [SKAction fadeAlphaTo:0 duration:0.3];
    SKAction *group = [SKAction group:@[scale, fade]];
    [self.player runAction:group];
    if ([self.player respondsToSelector:@selector(shadowCastBitMask)]) {
        self.player.shadowCastBitMask=0;
    }
    
    //    SKAction *wait = [SKAction waitForDuration:0.8];
    //    [self runAction:wait completion:^(void){
    [self showScoreScreen];
    //    }];
    
}
- (void)runColorChange
{
    
    if (flagCount%3==0) {
        ColorScheme *newscheme = [[ColorScheme alloc] initWithSchem:flagCount/5];
        
        UIColor *beginColor = self.backgroundColor;
        UIColor *destColor = newscheme.backgroundColor;
        
        
        CGFloat duration = 7;
        SKAction *act = [SKAction customActionWithDuration:duration actionBlock:^(SKNode *node, CGFloat elapsedTime){
            
            ((SKScene *)node).backgroundColor = [UIColor colorBeetweenColor:beginColor andColor:destColor factor:elapsedTime/duration];
            scheme.backgroundColor = ((SKScene *)node).backgroundColor;
            
        }];
        [self removeActionForKey:@"colorChange"];
        [self runAction:act withKey:@"colorChange"];
        
        beginColor = self.player.color;
        destColor = newscheme.squareColor;
        duration= 0.1;
        SKAction *act2 = [SKAction customActionWithDuration:duration actionBlock:^(SKNode *node, CGFloat elapsedTime){
            
            ((SKSpriteNode *)node).color = [UIColor beterColorBeetweenColor:beginColor andColor:destColor factor:elapsedTime/duration];
            scheme.squareColor = ((SKSpriteNode *)node).color;
        }];
        [self.player removeActionForKey:@"colorChange"];
        [self.player runAction:act2 withKey:@"colorChange"];
        
        
        
        
        
        //final action
        //        SKAction *act3 = [SKAction customActionWithDuration:duration actionBlock:^(SKNode *node, CGFloat elapsedTime){
        //
        //            ((ColorScheme *)node). = [UIColor colorBeetweenColor:beginColor andColor:destColor factor:elapsedTime/duration];
        //        }];
        //        [scheme runAction:act2 withKey:@"colorChange"];
        
    }
}
- (void)flagFound
{
    
    
    self.points++;
    flagCount++;
    if (mode != insane) {
        [self runColorChange];
    }
    
    
    //run sound
    
    
    
    
    SKLabelNode *pointsLabelTemp = [SKLabelNode node];
    pointsLabelTemp.text = [NSString stringWithFormat:@"%d", self.points];
    pointsLabelTemp.position = self.flag.position;
    pointsLabelTemp.alpha = 0;
    pointsLabelTemp.zPosition = 10;
    pointsLabelTemp.fontSize = 40;
    pointsLabelTemp.fontName = @"HelveticaNeue-Bold";
    pointsLabelTemp.fontColor = [UIColor colorWithCode:0xFEE600];
    [self.container addChild:pointsLabelTemp];
    
    SKAction *soundAction = [soundEffects valueForKey:[NSString stringWithFormat:@"%d.wav", 1+(flagCount)%7]];

    SKAction *appear = [SKAction fadeAlphaTo:0.95 duration:0.2];
    SKAction *dissapear = [SKAction fadeAlphaTo:0 duration:0.5];
    SKAction *chain = [SKAction sequence:@[appear, dissapear]];
    SKAction *scale = [SKAction scaleBy:1.3 duration:0.7];
    scale.timingMode = SKActionTimingEaseOut;
    SKAction *array = [SKAction group:@[chain, scale, soundAction]];
    [pointsLabelTemp runAction:array];
    
    
    SKLabelNode *tempToken = [self.flag copy];
    [self.container addChild:tempToken];
    
    SKAction *dis = [SKAction scaleTo:0 duration:0.5];
    dis.timingMode = SKActionTimingEaseIn;
    SKAction *fade = [SKAction fadeAlphaTo:0 duration:0.3];
    [tempToken runAction:[SKAction group:@[dis, fade]] completion:^(void){ [tempToken removeFromParent]; }];
    
//    NSString *emiterPath = [[NSBundle mainBundle] pathForResource:@"Radar" ofType:@"sks"];
//    
//    SKEmitterNode *emitter = [NSKeyedUnarchiver unarchiveObjectWithFile:emiterPath];
//    emitter.position = self.flag.position;
//    [self addChild:emitter];
//    [emitter resetSimulation];

    
    RingNode *rNode = [[RingNode alloc] init];
    rNode.position  = self.flag.position;
    [rNode test];
    
    [self.container addChild:rNode];
    
    [self addRandomFlag];
    if (mode != insane) {
        [self addRandomBug];
    }
    if (flagCount%5==0 && self.points>1 && self.powerups.count==0) {
        [self addPowerup];
    }
    int activeBugs = 0;
    for (int i=0; i<BUG_LIMIT; i++) {
        if (active[i]) {
            activeBugs++;
        }
    }
#if ISIPAD
    if (arc4random()%3==0 && activeBugs>15 && self.powerups.count==0) {
        [self addBomb];
    }

#else
    if (arc4random()%3==0 && activeBugs>11 && self.powerups.count==0) {
        [self addBomb];
    }

#endif

    
    int comboTime = 1;
    if (lastColectionTime) {
        NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:lastColectionTime];
        if (comboTimer) {
            [comboTimer invalidate];
        }
        if (interval<comboTime) {
            self.combo++;
            comboTimer = [NSTimer scheduledTimerWithTimeInterval:comboTime target:self selector:@selector(comboTimerFire:) userInfo:nil repeats:NO];
        }
        
    }else{
        self.combo = 1;
    }
    lastColectionTime = [NSDate date];
    
    
    
    //    [self addPowerup];
    //    self.player.size = CGSizeMake(20+self.points, 20+self.points);
    //    self.player = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithRed:0 green:0 blue:0.5 alpha:1 ] size:CGSizeMake(20+self.points, 20+self.points)];
    
}
#pragma mark --Combo
- (void)comboTimerFire: (id)userInfo{
    
    self.points += self.combo-1;
    self.combo=1;
}
-(void)addComboLabel
{
    comboLabel = [SKLabelNode node];
    comboLabel.text = @"| speed bonus 2";
    comboLabel.fontName = @"Thonburi";
    comboLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    comboLabel.fontSize = 10;
    comboLabel.position = CGPointMake(45, self.frame.size.height-35-10);
    comboLabel.fontColor = [UIColor colorWithCode:0xFFFFFF];
    comboLabel.alpha=0;
    
    [self addChild:comboLabel];
    NSTimeInterval animTime = 0.4;
    SKAction *action1 = [SKAction fadeAlphaTo:1 duration:animTime];
    SKAction *action2 = [SKAction moveByX:0 y:10 duration:animTime];

    [comboLabel runAction:[SKAction group:@[action1, action2]]];
}
- (void)removeComboLabel
{
    NSTimeInterval animTime = 0.3;
    SKAction *action1 = [SKAction fadeAlphaTo:0 duration:animTime];
    SKAction *action2 = [SKAction moveByX:-20 y:0 duration:animTime];
    [comboLabel runAction:[SKAction group:@[action1, action2]] completion:^(void){
        [comboLabel removeFromParent];
    }];


}
- (void)randomlySetPositionSprite: (SKSpriteNode *)node avoidObject:(NSArray *)obj
{
    //assuming anchor in center
    //avoid objects in array, objects sksprites
    int margin = 20;
    CGPoint randPoint;
    int dist = 100;
    int fuse = 10000;
    do {
        CGFloat dx;
        CGFloat dy;
        dx = margin + node.size.width/2.f + arc4random_uniform(self.size.width-margin*2-node.size.width);
        dy = margin + node.size.height/2.f + arc4random_uniform(self.size.height-margin*2-node.size.height);
        
        randPoint = CGPointMake(dx, dy);
        fuse--;
        if (!fuse) {
//            NSLog(@"Fuse broke on %@", node);
            break;
        }
    } while ([self minDistanceFromPoint:randPoint toObjects:obj] < dist);
    //    NSLog(@"Random times: %d", 10000-fuse);
    node.position = randPoint;
    
}
- (CGFloat)minDistanceFromPoint: (CGPoint)p toObjects: (NSArray *)obj //take array of sprites
{
    CGFloat min = 100000;
    for (SKSpriteNode *node in obj) {
        min = MIN(min, CGPointDistance(p, node.position));
    }
    return min;
}
- (void)addRandomPowerup
{
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    if (instr) {
        SKAction *action = [SKAction fadeAlphaTo:0 duration:0.5];
        [instr runAction:action completion:^(void){
            [instr removeFromParent];
        }];
    }
    UITouch *lastTouch;
    for (UITouch *touch in touches) {
        lastTouch = touch;
        //        touchdown = YES;
        touchLocation = [touch locationInNode:self];
        lastPoint = touchLocation;
        firstPoint = touchLocation;
        /*
         location.x *=2;
         location.y *=2;
         touchLocation = location;
         */
    }
    
    SKNode *node = [self nodeAtPoint:[touches.anyObject locationInNode:self]];
    
    if (!self.theGameIsAfoot) {
        if ([node.name isEqualToString:@"support_button"]) {
            
            handler = [[SupportMenuHandler alloc] init];
            
            UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Catch the Sun is, and always will be a free game without ads. If you would like to help me developing Catch the Sun and other games, consider writing a review on the store or buying me a cup of coffee." delegate:handler cancelButtonTitle:@"Close" destructiveButtonTitle:nil otherButtonTitles: @"Write a review", @"Buy the developer cup of coffee" , nil];
            [actionSheet showInView:self.view];
            
        }else if([node.name isEqualToString:@"leaderboards_button"]){
            GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
            if (localPlayer.authenticated) {
                [self.viewController showLeaderboard];
            }else
            {
                self.viewController.forceAutenticate = true;
                [self.viewController authenticateLocalPlayer];
            }
        }else if (endPanel && CGRectContainsRect(self.frame, endPanel.frame)) {
            
            SKAction *present = [SKAction moveToY:self.size.height+endPanel.size.height/2 duration:.25];
            present.timingMode = SKActionTimingEaseOut;
            
            [endPanel runAction:present completion:^(void){
                [self startGame];
            }];
        }else
            [self startGame];
    }
    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    velocity = CGVectorMake(0, 0);
    touchdown = NO;
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    for (UITouch *touch in touches) {
        touchdown = YES;
        
        lastPoint = touchLocation;
        
        CGPoint location = [touch locationInNode:self];
        //        location.x *=2;
        //        location.y *=2;
        touchLocation = location;
        
        
        //        NSLog(@"Location: %f %f First Point: %f %f", touchLocation.x, touchLocation.y, lastPoint.x, lastPoint.y);
        //        newpos = CGPointMake(fmin(location.x*2, self.size.width),
        //                                 fmin(location.y*2, self.size.height));
        
        //        self.player.position = newpos;
        
    }
}
- (CGMutablePathRef)generatePathFromCoordinates:(NSArray *)coordinates
{
    CGMutablePathRef path = CGPathCreateMutable();
    CGPoint point = [(NSValue *)[coordinates objectAtIndex:0] CGPointValue];
    CGPathMoveToPoint(path, nil, point.x, point.y);
    
    for (int i=1; i < coordinates.count; i++)
    {
        point = [(NSValue *)[coordinates objectAtIndex:i] CGPointValue];
        CGPathAddLineToPoint(path, nil, point.x, point.y);
    }
    
    return path;
}
-(void)update:(CFTimeInterval)currentTime {
    
    
    
    /* Called before each frame is rendered */
    CFTimeInterval timeSinceLast = currentTime - self.lastUpdateTimeInterval;
    self.lastUpdateTimeInterval = currentTime;
    if (timeSinceLast > 1) { // more than a second since last update
        timeSinceLast = 1.0 / 60.0;
        self.lastUpdateTimeInterval = currentTime;
    }
    
    
    
    
    //    if (self.player.position.x != touchLocation.x &&
    //        self.player.position.y != touchLocation.y && touchdown) {
    //
    //        int signX = self.player.position.x<touchLocation.x?1:-1;
    //        int signY = self.player.position.y<touchLocation.y?1:-1;
    //
    //        CGPoint newpos = CGPointMake(
    //                                     self.player.position.x + 1*signX,
    //                                     self.player.position.y + 1*signY);
    
    
    //        float speed = 10*timeSinceLast;
    //        newpos = CGPointMake( self.player.position.x + speed*(touchLocation.x-self.player.position.x ) ,
    //                             self.player.position.y  + speed*(touchLocation.y-self.player.position.y ));
    
    if(self.theGameIsAfoot)
    {
        
        CGVector oldVel = velocity;
        velocity = CGVectorMake(firstPoint.x-touchLocation.x, firstPoint.y-touchLocation.y);
        velocity=   CGVectorMultiplyByScalar(velocity, -0.5);
        //    velocity.dx/=1;
        //    velocity.dy/=1;
        firstPoint.x = firstPoint.x - (firstPoint.x-touchLocation.x)*0.15;
        firstPoint.y = firstPoint.y - (firstPoint.y-touchLocation.y)*0.15;
        
        
        if (CGVectorLength(velocity)>50) {
            velocity = CGVectorMultiplyByScalar(CGVectorNormalize(velocity), 50);
            
        }
        //    if (CGVectorLength(velocity)<4) {
        //        velocity = CGVectorMultiplyByScalar(CGVectorNormalize(velocity), 4);
        //    }
        
        int coef = 0;
        
        if (!touchdown) {
            coef = 0;
        }
        if (CGVectorLength(velocity)<CGVectorLength(CGVectorMultiplyByScalar(CGVectorNormalize(velocity), -coef))) {
            velocity.dx = 0;
            velocity.dy = 0;
        }else
            velocity = CGVectorSum(velocity, CGVectorMultiplyByScalar(CGVectorNormalize(velocity), -coef));
        
        
        int speed =  10;
        if((pow & slowmo) != 0)
            speed = 5;
        double vv = CGVectorLength(velocity);
        
        
        for (int i=0; i<MIN(bugNum, BUG_LIMIT); i++) {
            SKSpriteNode *bug = [self.bugs objectAtIndex:i];
            if (!active[i]) {
                continue;
            }
            CGPoint newpos = bug.position;
            
            
            if (CGPointDistance(self.player.position, bug.position)<((double)self.player.size.width+bug.size.width)/2) {
                NSLog(@"Game Over: %d", self.points);
                [self gameOver];
                return;
            }
            //        if (CGRectIntersectsRect(bug.frame, self.flag.frame)) {
            //            [self flagFond];
            //            self.points--;
            //        }
            
            
            newpos.x += bX[i]*timeSinceLast*speed;
            newpos.y += bY[i]*timeSinceLast*speed;
            int mrg = 5;
            if (newpos.x+bug.size.width/2-mrg>self.frame.size.width) {
                bX[i]=-1*abs(bX[i]);
            }
            if (newpos.x-bug.size.width/2+mrg<0) {
                bX[i] = 1*abs(bX[i]);
            }
            if (newpos.y+bug.size.height/2-mrg>self.frame.size.height) {
                bY[i]=-1*abs(bY[i]);
            }
            if (newpos.y-bug.size.height/2+mrg<0) {
                bY[i] = 1*abs(bY[i]);
            }
            
            bug.position = newpos;
            
            CGVector repel = CGVectorMake(self.player.position.x-newpos.x, self.player.position.y-newpos.y);
            if (CGVectorLength(repel)<bug.size.width*1.2 && vv>.5) {
                //            repel = CGVectorNormalize(repel);
                repel = CGVectorMultiplyByScalar(CGVectorNormalize(repel), bug.size.width*1.2-CGVectorLength(repel));
                repel = CGVectorMultiplyByScalar(repel, 0.25);
                
                velocity = CGVectorSum(velocity, repel);
            }
        }
        
        CGPoint newpos = CGPointMake(self.player.position.x + velocity.dx,
                                     self.player.position.y + velocity.dy);
        
        
        newpos.x = MIN(self.frame.size.width-10, MAX(10, newpos.x));
        newpos.y = MIN(self.frame.size.height-10, MAX(10, newpos.y));
        
        CGVector movV = CGVectorBetweenPoints(self.player.position, newpos);
        
#ifdef MINE
        self.player.zRotation = fmod(CGVectorAngle(movV), M_PI/2) ;
        //    }
#endif

        self.player.position = newpos;
        //CGRectIntersectsRect(self.player.frame, self.flag.frame)
        if (CGPointDistance(self.player.position, self.flag.position)<((double)self.player.size.width+self.flag.size.height)/2) {
            [self flagFound];
        }
        NSArray *removed = [NSArray array];
        for (SKSpriteNode *powerup in self.powerups) {
            if (CGPointDistance(self.player.position, powerup.position)<((double)self.player.size.width+powerup.size.height)/2) {
                if ([powerup isKindOfClass:[PowerupNode class]]) {
                    [self powerupColected:(PowerupNode *)powerup];
                    removed = [removed arrayByAddingObject:powerup];
                }else
                {
                    @throw [NSException exceptionWithName:@"Logic exception" reason:@"Wrong class of powerup" userInfo:nil];
                    
                }
            }
        }
        [self.powerups removeObjectsInArray:removed];
        if (mode == insane) {
            CGFloat odl = self.player.size.width;
            odl/=sqrtf(2);
            if (self.player.position.x<odl || self.player.position.y<odl || self.size.height-self.player.position.y<odl || self.size.width-self.player.position.x<odl) {
                [self gameOver];
                return;
            }
        }
        
        
    }//end of the game is afoot if
    
    //#ifdef MINE
    if (trail.count >= 5) {
        [trail removeLastObject];
    }
    [trail insertObject:[NSValue valueWithCGPoint:self.player.position] atIndex:0];
    
    if ([[NSProcessInfo processInfo] respondsToSelector:@selector(operatingSystemVersion)]) { //draw trail only if ios 8
        
        if (trailNode) {
            [trailNode removeFromParent];
        }
        
        trailNode = [SKShapeNode node];
        trailNode.path = [self generatePathFromCoordinates:trail];
        trailNode.lineWidth = 20;
        if ([trailNode respondsToSelector:@selector(lineJoin)]) {
            trailNode.lineJoin = kCGLineCapSquare;
        }
        trailNode.strokeColor = scheme.squareColor;
        trailNode.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:200];
        //    trailNode.alpha = 0.4;
        [self.container addChild:trailNode];
        
    }
    
    //#endif
    
}
CGFloat CGPointDistance(CGPoint p1, CGPoint p2)
{
    return sqrt(pow(p1.x-p2.x, 2)+ pow(p1.y-p2.y, 2));
}

@end
