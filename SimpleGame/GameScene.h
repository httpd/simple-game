
//
//  GameScene.h
//  SimpleGame
//

//  Copyright (c) 2014 IdeaStudio. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "PowerupNode.h"
#import "SupportMenuHandler.h"
@class ColorScheme;
@class GameViewController;
enum horizontalConstraints
{
    hright,
    hcenter,
    hleft,
};
enum verticalConstraint
{
    vtop,
    vcenter,
    vbottom,
};
enum gameMode
{
    standard = 0,
    insane,
    speedRun,
};
@interface GameScene : SKScene
{
    int touchdown;
    //insane mode
    enum gameMode mode;
    NSTimer *addingTimer;
    
    SKLabelNode *instr;
//    SKSpriteNode *invisibleBugNode;
    
    CGPoint touchLocation;
    int bX[1000000];
    int bY[1000000];
    bool active[10000];
    int bugNum;
    CGPoint firstPoint;

    CGPoint lastPoint;
    
    SKLabelNode *pointsLabel;
    SKLabelNode *topScoreLabel;
    SKLabelNode *comboLabel;
    BOOL newTopScore;
    enum powerup pow;
    CGVector velocity;
    
    NSMutableArray *trail;
    SKShapeNode *trailNode;
    SKLightNode* light;
    SKSpriteNode *endPanel;
    ColorScheme *scheme;
    SKTexture *bugTexture;
    SKTexture *bugTexture_insane;
    NSDate *lastColectionTime;
    NSTimer *comboTimer;
    NSMutableDictionary *soundEffects;
    
    CGSize ivar;
    int flagCount;
    
    SupportMenuHandler *handler;
}
@property (nonatomic, weak) GameViewController *viewController;

@property BOOL theGameIsAfoot;
@property SKSpriteNode *player;
@property SKSpriteNode *flag;
@property SKSpriteNode *container;
@property NSArray *bugs;
@property NSMutableArray *powerups;
@property (nonatomic) int points;
@property (nonatomic) int combo;

@property (nonatomic) int topScore;
@property (nonatomic) int controllMode;
@property CFTimeInterval lastUpdateTimeInterval;
- (void)startGame;
@end
